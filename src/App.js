import React from 'react'
import './App.css'
import BookShelf from './Components/Home/BookShelf/BookShelf';
import Search from './Components/Search/Search';
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import {getAll} from './BooksAPI.js';

class BooksApp extends React.Component {
  constructor(){
    super();
    this.state={
        books:[]
             };
}

componentDidMount()
{
   getAll()
   .then((Response)=>{
       this.setState({books:Response})
   })
}
  render() {
    return (
      <div className="app">
           <BrowserRouter>
              <Switch>
                  <Route path='/' exact render={prop=><BookShelf data={this.state.books}/>} />
                  <Route path='/search' exact render={prop=><Search data={this.state.books}/>} />
              </Switch>                
           </BrowserRouter>
      </div>
    )
  }
}

export default BooksApp