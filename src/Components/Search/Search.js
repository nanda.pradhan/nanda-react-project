import React, { Component } from 'react';
import './Search.css';
import { NavLink } from 'react-router-dom'
import Books from '../Home/BookShelf/BookList/Book/Book';
import {search} from '../BooksAPI';
import {update} from '../BooksAPI';

class Search extends Component
{
    constructor(props){
        super(props);
        this.state={
           books:[],
           message:"Enter key to search books",
                   };
            }

    Search=(event)=>{
        var source=this;
        var books_array=[];
        var query=document.getElementById("query").value
        if(query===""){
            this.setState({books:[],message:"Please Enter search query"})
        }
        else
        {
        search(query).then((book_objects)=>{
            var source=this;
               book_objects.forEach(function(element) {
                   source.props.data.forEach((BookObj)=>{
                       if(BookObj.id===element.id)
                       {
                          element.shelf=BookObj.shelf
                       }
                    });
                       element.shelf=element.shelf?element.shelf:'none';
                       books_array.push(element)
                })
        }).then(()=>{  source.setState({books:books_array})}).catch(()=>{
                            source.setState({books:[],message:"No Books Found"})
                        })
        }
        }
     change=(book,value)=>
     {      
         book.shelf=value;
         update(book,value)
         var book_state=this.state.books;
         book_state.forEach((book_object)=>{
             if(book_object.id===book.id)
             {   
               this.setState({books:book_state});   
             }
         })  
    }

    render(){
        let booksDisp=[];
        var source=this; 
        return(
            <div>
            {
                 <div className="search-books">
                     <div className="search-books-bar">
                        <NavLink to="/" className="close-search"></NavLink>
                            <div className="search-books-input-wrapper">
                    <input type="text" placeholder="Search by title or author" id="query" onChange={this.Search}/>
                  </div>
                </div>
                <div className="search-books-results">
                <ol className="books-grid">
                {
                            source.state.books.map((data)=>{
                                booksDisp.push(<li key={data.id}><Books book={data} event={this.change}/></li>)
                            return undefined})
                } 
               
                {
                   (booksDisp.length!==0)?(booksDisp):(<li>{this.state.message}</li>) 
                }
                </ol>
                </div>
              </div>
            }
            </div>
        );
    }
}
export default Search;
