import React, { Component } from 'react';
import './SearchResult.css';
import Books from '../Home/BookShelf/BookList/Book/Book';

class SearchResult extends Component
{
    constructor(){
        super();
        this.state={
           books:[]
        
                 };
    }
    componentWillMount()
    {
        this.setState({books:this.props.books})
    }
    render(){
       
       
        let booksDisp=[];
        var source=this;
        return(
            <div>
            {
                (source.props.book.length>0)?(
                      this.props.book.map((data)=>{
                      booksDisp .push(<li> <Books obj={data} event={this.change}/></li>)
                      return undefined})

                ):(<li>No Books Found</li>)

          

         } 
          {booksDisp}
         </div>

        );
    }
}
export default SearchResult