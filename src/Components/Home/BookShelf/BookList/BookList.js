import React, { Component } from 'react';
import './BookList.css';
import Books from './Book/Book';

class BookShelf extends Component
{
    render(){
        let books=[];
        return(
            <div className="bookshelf">
            <h2 className="bookshelf-title">{this.props.children}</h2>
            <div className="bookshelf-books">
            <ol className="books-grid">
               {
                this.props.books.map((data)=>{
                books.push(<li key={data.id}><Books book={data} event={this.props.event}/></li>)
                return undefined})
               } 
               {books}
            </ol>
            </div>
            </div>
        );
    }
}
export default BookShelf;