import React, { Component } from 'react';
import './Book.css';
import BookTop from './BookTop';
import BookTitle from './BookTitle';
import BookAuthor from './BookAuthor';

class Book extends Component
{
    render(){
        return(
            <div className="book">
            <BookTop book={this.props.book} event={this.props.event}/>
            <BookTitle book={this.props.book}/>
            <BookAuthor book={this.props.book} />
            </div>
        );
    }
}
export default Book