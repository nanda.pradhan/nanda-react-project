import React, { Component } from 'react';
import './BookTop.css';
import BookShelfChanger from './BookShelfChanger';

class BookTop extends Component
{
    render(){
        var url = (this.props.book.imageLinks) ? (this.props.book.imageLinks.smallThumbnail):('https://bit.ly/2K0KOyi')
        return(
            <div className="book-top">
                <div className="book-cover" style={{ width: 128, height: 193, backgroundImage: 'url('+url+')' }}></div>
                <BookShelfChanger book={this.props.book} event={this.props.event}/>
            </div>

        );
    }
}
export default BookTop;
