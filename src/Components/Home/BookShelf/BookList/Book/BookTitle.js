import React, { Component } from 'react';
import './BookTitle.css';

class BookTitle extends Component
{
    render(){
        return(
            <div className="book-title">{this.props.book.title}</div>
        );
    }
}
export default BookTitle;