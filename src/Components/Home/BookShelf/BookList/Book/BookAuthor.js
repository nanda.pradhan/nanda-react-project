import React, { Component } from 'react';
import './BookAuthor.css';

class BookAuthor extends Component
{
    render(){  
        return(
            <div className="book-authors">{this.props.book.authors}</div>
        );
    }
}
export default BookAuthor;