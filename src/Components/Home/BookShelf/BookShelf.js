import React, { Component } from 'react';
import './BookShelf.css';
import Title from './Title/Title';
import BookList from './BookList/BookList';
import {update} from '../BooksAPI.js';
import { NavLink } from 'react-router-dom'


class ListBooks extends Component
{
    constructor(props)
    {
        super(props);
        this.state={
           shalves:{currentlyReading:[],wantToRead:[],read:[]}
                   };
    }

    change=(book,value)=>{
        var source_shelf_name=book.shelf
            var source_shelf=this.state.shalves[source_shelf_name];
            var destination_shelf_name=value;
        if(value!=="none")
        {
            book.shelf=value;
            var destination_shelf=this.state.shalves[destination_shelf_name];
            destination_shelf.push(book)   
        }
        var index=0;
        source_shelf.forEach((book_object)=>{
            if(book_object.id===book.id)
            {
                source_shelf.splice(index,1)
            }
            index+=1;
        })
        update(book,destination_shelf_name)
        this.setState({shalve:{source_shelf_name:source_shelf,destination_shelf_name:destination_shelf}}); 
      }

    getData(prop)
    { 
       var Currentlyreading=[];
       var WanttoRead=[]
       var Read=[]
            prop.data.forEach(element => {
               if(element.shelf==='currentlyReading'){
                   Currentlyreading.push(element)
               }
               else if(element.shelf==='wantToRead') {
                WanttoRead.push(element)
               }
               else if(element.shelf==='read'){
                    Read.push(element)

               }
            });
            var shalve={currentlyReading:Currentlyreading,wantToRead:WanttoRead,read:Read}
            this.setState({shalves:shalve}) 
    }

    componentWillReceiveProps(prop)
    {
        this.getData(prop)
    }
    componentDidMount(props)
    {
     this.getData(this.props)
    }

    render(){      
        return(
            <div className="list-books">
            <Title />
             <BookList books={this.state.shalves.currentlyReading} event={this.change}>Currently Reading</BookList>
            <BookList books={this.state.shalves.wantToRead} event={this.change}>Want To Read</BookList>
            <BookList books={this.state.shalves.read} event={this.change}>Read</BookList>
            <div className="open-search">
            <NavLink  to="/search"></NavLink>
             </div> 
             </div>
        );
    }
}
export default ListBooks;